#!/bin/bash
# Setup Jenkins Project
# REPO: https://homework-gitea.apps.shared.na.openshift.opentlc.com/voravit-redhat.com/ocp4_app_deploy_homework.git

if [ "$#" -ne 3 ]; then
    echo "Usage:"
    echo "  $0 GUID REPO CLUSTER"
    echo "  Example: $0 wkha https://github.com/redhat-gpte-devopsautomation/advdev_homework_template.git shared.na.openshift.opentlc.com"
    exit 1
fi

# USERID=$(oc whoami 2>/dev/null)
# if [ $? -ne 0 ];
# then
#   echo "You need to login to OpenShift before run this script"
#   exit 1
# else
#   echo "########## Hello, $USERID ##########"
# fi

GUID=$1
REPO=$2
CLUSTER=$3
JENKINS_VOLUME_CAPACITY=4Gi
JENKINS_MEMORY_LIMIT=2Gi
JENKINS_MEMORY_REQUEST=1Gi
JENKINS_CPU_LIMIT=2
JENKINS_CPU_REQUEST=500m
JENKINS_SLAVE_NAME=jenkins-agent-appdev
JENKINS_ENABLE_OAUTH=true
JENKINS_DISABLE_ADMINISTRATIVE_MONITORS=true

echo "Setting up Jenkins in project ${GUID}-jenkins from Git Repo ${REPO} for Cluster ${CLUSTER}"

# oc get project/${GUID}-jenkins 1>/dev/null 2>&1
# if [ $? -ne 0 ];
# then
#   echo "Create Project ${GUID}-jenkins"
#   oc new-project ${GUID}-jenkins --display-name="Jenkins"
# else
#   echo "Project ${GUID}-jenkins alrady exist!!"
# fi
# Set up Jenkins with sufficient resources
# DO NOT FORGET TO PASS '-n ${GUID}-jenkins to ALL commands!!'
# You do not want to set up things in the wrong project.
echo "Create Jenkins with Presistent Volume"
oc new-app jenkins-persistent \
--param ENABLE_OAUTH=$JENKINS_ENABLE_OAUTH \
--param JENKINS_SERVICE_NAME=jenkins \
--param JNLP_SERVICE_NAME=jenkins-jnlp \
--param NAMESPACE=openshift \
--param JENKINS_IMAGE_STREAM_TAG=jenkins:2 \
--param MEMORY_LIMIT=$JENKINS_MEMORY_LIMIT \
--param VOLUME_CAPACITY=$JENKINS_VOLUME_CAPACITY \
--param DISABLE_ADMINISTRATIVE_MONITORS=$JENKINS_DISABLE_ADMINISTRATIVE_MONITORS \
-n ${GUID}-jenkins 
# oc process -f jenkins-persistent.yaml \
# --param ENABLE_OAUTH=$JENKINS_ENABLE_OAUTH \
# --param JENKINS_SERVICE_NAME=jenkins \
# --param JNLP_SERVICE_NAME=jenkins-jnlp \
# --param NAMESPACE=openshift \
# --param JENKINS_IMAGE_STREAM_TAG=jenkins:2 \
# --param MEMORY_LIMIT=$JENKINS_MEMORY_LIMIT \
# --param VOLUME_CAPACITY=$JENKINS_VOLUME_CAPACITY \
# --param DISABLE_ADMINISTRATIVE_MONITORS=$JENKINS_DISABLE_ADMINISTRATIVE_MONITORS |
# oc create -n ${GUID}-jenkins -f -

echo "Set Jenkins Request/Limit"
oc set resources dc jenkins \
--requests=memory=$JENKINS_MEMORY_REQUEST,cpu=$JENKINS_CPU_REQUEST \
--limits=memory=$JENKINS_MEMORY_LIMIT,cpu=$JENKINS_CPU_LIMIT \
-n ${GUID}-jenkins 

# Create custom agent container image with skopeo.
# Build config must be called 'jenkins-agent-appdev' for the test below to succeed
# oc apply -f $JENKINS_SLAVE_NAME-bc.yml -n $GUID-jenkins
# # oc new-build $JENKINS_SLAVE_NAME --strategy=docker --binary -n ${GUID}-jenkins
# oc start-build $JENKINS_SLAVE_NAME -n ${GUID}-jenkins
echo "Create Jenkins Slave:$JENKINS_SLAVE_NAME"
oc new-build --strategy=docker --dockerfile=$'FROM quay.io/openshift/origin-jenkins-agent-maven:4.1.0\n
   USER root\n
   RUN curl https://copr.fedorainfracloud.org/coprs/alsadi/dumb-init/repo/epel-7/alsadi-dumb-init-epel-7.repo -o /etc/yum.repos.d/alsadi-dumb-init-epel-7.repo && \ \n
   curl https://raw.githubusercontent.com/cloudrouter/centos-repo/master/CentOS-Base.repo -o /etc/yum.repos.d/CentOS-Base.repo && \ \n
   curl http://mirror.centos.org/centos-7/7/os/x86_64/RPM-GPG-KEY-CentOS-7 -o /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7 && \ \n
   DISABLES="--disablerepo=rhel-server-extras --disablerepo=rhel-server --disablerepo=rhel-fast-datapath --disablerepo=rhel-server-optional --disablerepo=rhel-server-ose --disablerepo=rhel-server-rhscl" && \ \n
   yum $DISABLES -y --setopt=tsflags=nodocs install skopeo && yum clean all\n
   USER 1001' \
--name=$JENKINS_SLAVE_NAME \
-n ${GUID}-jenkins


# Create Secret with credentials to access the private repository
# You may hardcode your user id and password here because
# this shell scripts lives in a private repository
# Passing it from Jenkins would show it in the Jenkins Log
oc create secret generic gogs-secret \
--from-literal=username=voravit-redhat.com \
--from-literal=password=password2323 \
-n ${GUID}-jenkins



# Create pipeline build config pointing to the ${REPO} with contextDir `openshift-tasks`
# Build config has to be called 'tasks-pipeline'.
# Make sure you use your secret to access the repository
echo "apiVersion: build.openshift.io/v1
kind: BuildConfig
metadata:
  labels:
    build: tasks-pipeline
  name: tasks-pipeline
spec:
  failedBuildsHistoryLimit: 5
  nodeSelector: {}
  output: {}
  postCommit: {}
  resources: {}
  runPolicy: Serial
  source:
    contextDir: openshift-tasks
    git:
      ref: master
      uri: ${REPO}
    sourceSecret:
      name: gogs-secret
    type: Git
  strategy:
    jenkinsPipelineStrategy:
      jenkinsfilePath: Jenkinsfile
    type: JenkinsPipeline
  successfulBuildsHistoryLimit: 5
  triggers: []
status:
  lastVersion: 1" | oc apply -f - -n ${GUID}-jenkins



# ========================================
# No changes are necessary below this line
# Make sure that Jenkins is fully up and running before proceeding!
while : ; do
  echo "Checking if Jenkins is Ready..."
  AVAILABLE_REPLICAS=$(oc get dc jenkins -n ${GUID}-jenkins -o=jsonpath='{.status.availableReplicas}')
  if [[ "$AVAILABLE_REPLICAS" == "1" ]]; then
    echo "...Yes. Jenkins is ready."
    break
  fi
  echo "...no. Sleeping 10 seconds."
  sleep 10
done

# Make sure that Jenkins Agent Build Pod has finished building
while : ; do
  echo "Checking if Jenkins Agent Build Pod has finished building..."
  AVAILABLE_REPLICAS=$(oc get pod jenkins-agent-appdev-1-build -n ${GUID}-jenkins -o=jsonpath='{.status.phase}')
  if [[ "$AVAILABLE_REPLICAS" == "Succeeded" ]]; then
    echo "...Yes. Jenkins Agent Build Pod has finished."
    break
  fi
  echo "...no. Sleeping 10 seconds."
  sleep 10
done

